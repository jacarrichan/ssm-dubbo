package com.jacarrichan.demo.lemur.service;

import com.jacarrichan.demo.lemur.models.CatVo;

public interface CatService extends BaseService<CatVo, Integer> {

}
