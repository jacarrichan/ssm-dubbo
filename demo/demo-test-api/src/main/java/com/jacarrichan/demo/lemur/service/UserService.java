package com.jacarrichan.demo.lemur.service;

import com.jacarrichan.demo.common.service.CommonService;
import com.jacarrichan.demo.lemur.models.UserVo;

public interface UserService extends CommonService<UserVo, Integer> {

}
