# 前言
本工程是在[关掉耳朵](http://git.oschina.net/rundonkey) 童鞋的劳动成果基础上改造而来。 

# 读者须知
## 技术方面
1. 本项目假设阅读者 能够使用maven，简单了解Zookeeper、dubbo及流行的springMVC+mybatis框架组合；

1. maven生命周期：https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
1. dubbo官网文档地址： http://dubbo.io/User+Guide-zh.htm
1. Zookeeper下载地址：https://zookeeper.apache.org/releases.html
1. spring文档地址：http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/
1. mybatis文档地址：http://www.mybatis.org/
1. 如果你只用dubbo，就不要去折腾dubbo的源码
1. 快速打开某个类，eclipse快捷键是ctrl+shift+h，idea是连按两次shift;

## 工程方面
1. 工程使用lombok插件了，用来快速生成get/set方法和slf4j变量注入，所以请自行给你的IDE装上lombok插件， 不然代码会报错。
1. 介于部分开发者机器上没有MySQL数据库，因此使用H2这一嵌入式数据库，但服务实现的项目的pom.xml文件中mybatis-generator插件及其配置文件generatorConfig.xml依然保留对MySQL的连接;
1. 项目开发时使用的dubbo-monitor和Zookeeper的下载地址： http://download.csdn.net/download/jacarri/9500004

*代码中有log.debug(“test”)，但没有定义log这个属性，是因为使用@slf4j注解，lombok插件会帮你处理，所以如果你IDE没有装lombok插件代码就会报错*

#工程说明

本项目演示了如何使用流行的springMVC、mybatis、dubbo搭建分布式环境

1.  dubbo的服务感知有很多方式：直连、广播、redis、Zookeeper，本项目只采用Zookeeper这个注册中心


# 结构说明
```

-demo-parent (用来定义演示项目的依赖版本的)
-demo-test  (演示项目)
    -demo-common (通用的接口实现)
    -demo-common-api (通用的接口)
    -demo-test-api  (存放接口和接口所需要的参数bean)
    -demo-test-service (服务的实现，dubbo中的Provider)
    -demo-test-views (web项目，dubbo中的consumer)

```


*  在demo根目录中pom定义依赖版本会导致循环依赖，所以单独其一个项目;
*  建议所有XXX-api项目依赖demo-common-api，所有的XXX-service项目都依赖demo-common;

# demo开发
1. 将项目导进eclipse后，解决项目编译问题等其它错误；
1. 执行[com.jacarrichan.demo.test.service.UserServiceTest.testFindByQuery()]这个单元测试，确认从service到数据库都配置无误。




# demo的运行
1. 在本地把Zookeeper跑起来，我用的是zookeeper-3.4.6，windows系统中执行bin目录的zkServer.cmd即可;   
1. 执行/demo-test-service/src/main/java/com/jacarrichan/demo/test/service/start/Bootstraper.java的main方法，将Provider跑起来；
1. 将dubbo-monitor-simple-2.4.10跑起来，使用浏览器访问其界面，在services选项卡中查看上面的Provider中的服务是否已经注册到注册中心；
1. 执行/demo-test-views/src/test/java/com/jacarrichan/demo/test/tools/XmlJettyServer.java的main方法，将这个web项目跑起来。浏览器访问"http://localhost:8080/hello/helloworld.html" ，或者  "http://localhost:8080/hello/helloworld.json"， springMVC中的内容协商管理器会根据请求后缀使用合适的视图渲染器，目前只配置了html(freemaker)和json(jackson)，

*跑前端web项目只需要运行一个main方法就行了，不需要部署到tomcat里面*；

* 上述过程总结如下：
![](docs/img/procedure_of_project.jpg)


# 项目部署

1. Registry: 生产环境中至少3个Zookeeper实例组成集群,放在不同的服务器上;
1. web项目/Consumer:生产环境中，一般将demo-test-views这样的web项目部署在jetty或tomcat或者jboss等Servlet容器中，如果引入了spring-boot，即使是web项目也能很方便的以jar方式启动;
1. 后端服务/provider: 将demo-test-service这样的服务以jar的方式启动，即Standalone方式。改造下放在servlet容器中也可;

*所以大家不要拘泥于项目是以何种方式启动的，你喜欢用哪个就用哪个，你能用哪个搞定就用哪个搞定*

![dubbo架构]( http://dubbo.io/dubbo-architecture.jpg-version=1&modificationDate=1330892870000.jpg )

# 架构探讨
## dubbo的局限
 任何技术都有其使用范围和局限性，dubbo也不例外。dubbo主要是对Provider提供的服务进行服务治理、负载均衡、灰度发布提供支撑；不负责consumer对外提供的服务，我们一般用nginx或者F5来实现它的负载均衡。nginx提供热reload，这样已经够用了。如果要对nginx实现负载均衡的话，可以使用DNS的方式。

## 关于dubbo与dubbox
 我在项目中没有使用过dubbox。dubbox可以直接将服务以REST方式暴露，根据dubbox官网了解，如果consumer是非java语言，dubbox自身的软负载会失效，需要自行实现负载均衡机制；再加上maven中央仓库没有dubbox的构建，所以我不是很喜欢dubbox。如果要实现REST，我一般会使用springMVC ，同时这样对以前有过springMVC经验的人也友好些.

# 常见问题
## alibabatech网站下线导致Eclipse无法读取xsd文件
### 问题症状

```
 Failed to read schema document 'http://code.alibabatech.com/schema/dubbo/dubbo.xsd', because 1) could not find the document; 2) the document could not be read; 3) the root element of the document is not <xsd:schema>
```

### 解决办法

#### 添加host信息
系统hosts文件添加如下信息:

> 127.0.0.1 code.alibabatech.com

完毕后，ping这个域名，应该得到如下信息:

```
Microsoft Windows [版本 6.3.9600]
(c) 2013 Microsoft Corporation。保留所有权利。

C:\Users\Jacarri>ping code.alibabatech.com

正在 Ping code.alibabatech.com [127.0.0.1] 具有 32 字节的数据:
来自 127.0.0.1 的回复: 字节=32 时间<1ms TTL=128
来自 127.0.0.1 的回复: 字节=32 时间<1ms TTL=128
来自 127.0.0.1 的回复: 字节=32 时间<1ms TTL=128
来自 127.0.0.1 的回复: 字节=32 时间<1ms TTL=128

127.0.0.1 的 Ping 统计信息:
    数据包: 已发送 = 4，已接收 = 4，丢失 = 0 (0% 丢失)，
往返行程的估计时间(以毫秒为单位):
    最短 = 0ms，最长 = 0ms，平均 = 0ms

```

* windows系统的hosts文件地址：C:\Windows\System32\drivers\etc

#### nginx配置反向代理

在nginx配置文件目录nginx-1.6.2\conf\sites-available,添加xsd.conf，另外请自行确定nginx启动时会读取该配置文件

```
server {
    listen 80;
    server_name code.alibabatech.com;

    # individual nginx logs for this web vhost
    access_log  /var/log/nginx/code.alibabatech.com/access.log;
    error_log   /var/log/nginx/code.alibabatech.com/error.log ;
    
    #when not specify request uri, redirect to /index;
    location  / {
        root /home/admin/project/staticFile;
    } 
}

```

* 请在nginx当前分区中创建[/var/log/nginx/code.alibabatech.com/]文件夹，用于存放日志；
* 同上分区中创建[/home/admin/project/staticFile]文件夹，用于存放xsd文件；

#### 存放xsd文件，并启动nginx测试
根据上文nginx配置，已经Eclipse报错信息，作如下推定：

* Eclipse 需要访问'http://code.alibabatech.com/schema/dubbo/dubbo.xsd' ;
* nginx配置中将 code.alibabatech.com映射到了[/home/admin/project/staticFile]目录;
* 也就说需要如下路径有xsd文件[/home/admin/project/staticFile/schema/dubbo/dubbo.xsd]

把dubbo.jar中[ /META-INF/dubbo.xsd]文件找出来放在上述地址中，启动nginx，然后用浏览器访问测试下。

* 如果访问不到，请根据access.log和error.log里面的信息确认是何种问题.



